from flask import Flask, render_template, request
import numpy as np
import cv2

from braille_recogn import *


app = Flask(__name__)

@app.route('/')
def main_loop():
    return render_template('braille_app.html')

@app.route('/recognize', methods=['POST'])
def recognize():
    file = request.files['file']
    f = file.read()

    image=cv2.imdecode(np.frombuffer(f, np.uint8), flags=-1)
        
    allCoord, im2 = findCircles(image)

    finalCoord=rectIntersection(allCoord)
    brailleCoord=mergesort(finalCoord,0)

    brailleCoord=removeFloatingPoints(brailleCoord)

    brailleCoord=removeNoise(brailleCoord)
    # got to get rid of noise check
    splitCoord=(splitIntoBraille(brailleCoord))
    # determine if parts are missing
    # showCircles(image,brailleCoord)
    
    brailleStrList=(convCoordToStr(im2,splitCoord))
  

    # print(file.read(), file=sys.stderr)

    if request.form['language'] == 'russian':
        constBrailleDict = constBrailleDict_rus
    else:
        constBrailleDict = constBrailleDict_eng

    return getAlphaString(brailleStrList, constBrailleDict)  